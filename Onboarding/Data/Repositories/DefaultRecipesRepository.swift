//
//  DefaultRecipesRepository.swift
//  Onboarding
//
//  Created by Андрей Калямин on 30.05.2022.
//

import Foundation

final class DefaultRecipesReposytory: RecipesRepository {
    private var dataTransferService: DataTransferService
    
    init(dataTransferService: DataTransferService) {
        self.dataTransferService = dataTransferService
    }
    
    func fetchRecipesList(query: RecipeQuery,
                          offset: Int,
                          number: Int,
                          completion: @escaping (Result<RecipesPage, Error>) -> Void) -> Cancellable? {
        
        let requestDTO = RecipesRequestDTO(query: query.query, offset: offset, number: number)
        let task = RepositoryTask()
        
        let endpoint = APIEndpoints.getRecipes(with: requestDTO)
        task.networkTask = self.dataTransferService.request(with: endpoint) { result in
            switch result {
            case .success(let responseDTO):
                completion(.success(responseDTO.toDomain()))
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
        return task
    }
}

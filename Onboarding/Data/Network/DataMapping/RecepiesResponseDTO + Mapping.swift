//
//  RecipesResponseDTO.swift
//  RecipesList
//
//  Created by Андрей Калямин on 15.07.2021.
//

import Foundation
import CloudKit

struct RecipesResponseDTO: Decodable {
    private enum CodingKeys: String, CodingKey {
        case page = "offset"
        case totalPages = "totalResults"
        case recipes = "results"
    }
    let page: Int
    let totalPages: Int
    let recipes: [ReceptDTO]
    
}

extension RecipesResponseDTO {
    struct ReceptDTO: Decodable {
        let id: Int
        let title: String?
    }
}

extension RecipesResponseDTO {
    func toDomain() -> RecipesPage {
        let page = page/DefaultRecipesListViewModel.itemsPerPage
        let totalPages = totalPages/DefaultRecipesListViewModel.itemsPerPage
        return .init(page: page, totalPages: totalPages, recipes: recipes.map { $0.toDomain()})
    }
}

extension RecipesResponseDTO.ReceptDTO {
    func toDomain() -> Recipe {
        return .init(id: Recipe.Identifier(id), title: self.title)
    }
}



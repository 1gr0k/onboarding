//
//  APIEndpoints.swift
//  Onboarding
//
//  Created by Андрей Калямин on 29.05.2022.
//

import Foundation

struct APIEndpoints {
    static func getRecipes(with recipesRequestDTO: RecipesRequestDTO) -> Endpoint<RecipesResponseDTO> {
        
        return Endpoint(path: "recipes/complexSearch",
                        method: .get,
                        queryParametersEncodable: recipesRequestDTO)
    }
}

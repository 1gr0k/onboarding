//
//  Recipe.swift
//  Onboarding
//
//  Created by Андрей Калямин on 29.05.2022.
//

import Foundation

struct Recipe: Equatable, Identifiable {
    typealias Identifier = String
    let id: Identifier
    let title: String?
}

struct RecipesPage: Equatable {
    let page: Int
    let totalPages: Int
    let recipes: [Recipe]
}

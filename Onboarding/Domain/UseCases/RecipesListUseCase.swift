//
//  RecipesListUseCase.swift
//  Onboarding
//
//  Created by Андрей Калямин on 29.05.2022.
//

import Foundation

protocol RecipesListUseCase {
    func execute(requestValue: RecipesUseCaseRequestValue,
                 completion: @escaping (Result<RecipesPage, Error>) -> Void) -> Cancellable?
}

final class DefaultRecipesListUseCase: RecipesListUseCase {
    
    private var recipiesRepository: RecipesRepository
    
    init(recipiesRepository: RecipesRepository) {
        self.recipiesRepository = recipiesRepository
    }
    
    func execute(requestValue: RecipesUseCaseRequestValue,
                 completion: @escaping (Result<RecipesPage, Error>) -> Void) -> Cancellable? {

        
        return recipiesRepository.fetchRecipesList(query: requestValue.query,
                                                    offset: requestValue.offset,
                                                    number: requestValue.number,
                                                    completion: { result in
            var resultWithFav = result
            if case .success = result {
            resultWithFav = result.map { recipesPage in
                RecipesPage(page: recipesPage.page, totalPages: recipesPage.totalPages, recipes: recipesPage.recipes.map { recept in
                        Recipe(id: recept.id, title: recept.title)
                    })
                }
            }
            completion(resultWithFav)
        })
    }
}


struct RecipesUseCaseRequestValue {
    let query: RecipeQuery
    let offset: Int
    let number: Int = 10
    
    init(query: RecipeQuery, page: Int) {
        self.query = query
        self.offset = page*10
    }
}

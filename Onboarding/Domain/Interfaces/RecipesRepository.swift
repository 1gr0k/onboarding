//
//  RecipesRepository.swift
//  Onboarding
//
//  Created by Андрей Калямин on 29.05.2022.
//

import Foundation

protocol RecipesRepository {
    @discardableResult
    func fetchRecipesList(query: RecipeQuery, offset: Int, number: Int,
                           completion: @escaping (Result<RecipesPage, Error>) -> Void) -> Cancellable?
}

//
//  RecipesSceneDIContainer.swift
//  Onboarding
//
//  Created by Андрей Калямин on 29.05.2022.
//

import UIKit

final class RecipesSceneDIContainer {
    
    struct Dependencies {
        let apiDataTransferService: DataTransferService
    }
    
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - Use Cases
    func makeRecipesListUseCase() -> RecipesListUseCase {
        return DefaultRecipesListUseCase(recipiesRepository: makeResipesRepository())
    }
    
    // MARK: - Repositories
    func makeResipesRepository() -> RecipesRepository {
        return DefaultRecipesReposytory(dataTransferService: dependencies.apiDataTransferService)
    }
    
    // MARK: - Recipes List
    func makeRecipesListViewController() -> RecipesListViewController {
        let viewModel = makeRecipesListViewModel()
        let vc = RecipesListViewController.create(with: viewModel)
        viewModel.setDelegate(with: vc)
        
        return vc
    }
    
    func makeRecipesListViewModel() -> RecipesListViewModel {
        return DefaultRecipesListViewModel(recipesUseCase: makeRecipesListUseCase())
    }
    
    // MARK: - Flow Coordinators
    func makeMoviesSearchFlowCoordinator(navigationController: UINavigationController) -> RecipesListFlowCoordinator {
        return RecipesListFlowCoordinator(navigationController: navigationController,
                                           dependencies: self)
    }
}

extension RecipesSceneDIContainer: RecipesListFlowCoordinatorDependencies {}

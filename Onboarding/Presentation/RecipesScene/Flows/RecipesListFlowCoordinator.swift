//
//  RecipesListFlowCoordinator.swift
//  Onboarding
//
//  Created by Андрей Калямин on 29.05.2022.
//

import Foundation
import UIKit

protocol RecipesListFlowCoordinatorDependencies {
    func makeRecipesListViewController() -> RecipesListViewController
}

final class RecipesListFlowCoordinator {
    
    private weak var navigationController: UINavigationController?
    private let dependencies: RecipesListFlowCoordinatorDependencies
    
    init(navigationController: UINavigationController,
         dependencies: RecipesListFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    func start() {
        let vc = dependencies.makeRecipesListViewController()
        
        navigationController?.pushViewController(vc, animated: true)
    }
}

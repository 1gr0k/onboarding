//
//  RecipesListViewController.swift
//  Onboarding
//
//  Created by Андрей Калямин on 29.05.2022.
//

import UIKit

final class RecipesListViewController: UIViewController {
    
    private lazy var contentView: RecipesListView = {
       let view = RecipesListView()
        
        return view
    }()
    
    private var viewModel: RecipesListViewModel! = nil
    
    static func create(with viewModel: RecipesListViewModel) -> RecipesListViewController {
        let vc = RecipesListViewController()
        vc.viewModel = viewModel
        return vc
    }
    
    override func loadView() {
        self.view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
    }
}

extension RecipesListViewController: RecipesListViewModelDelegate {
    func fetchRecipes(didFetchRecipes items: [RecipesListItemViewModel]) {
        contentView.bind(with: items)
    }
}

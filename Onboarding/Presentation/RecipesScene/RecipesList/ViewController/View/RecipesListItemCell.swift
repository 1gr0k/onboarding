//
//  RecipesListItemCell.swift
//  Onboarding
//
//  Created by Андрей Калямин on 30.05.2022.
//

import UIKit
import SnapKit

final class RecipesListItemCell: UITableViewCell {
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 16.0)
        
        return label
    }()
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func bind(with model: RecipesListItemViewModel) {
        titleLabel.text = model.title
    }
    
    private func setup() {
        setupTitle()
    }
    
    private func setupTitle() {
        addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}

//
//  RecipesListView.swift
//  Onboarding
//
//  Created by Андрей Калямин on 29.05.2022.
//

import UIKit
import SnapKit

final class RecipesListView: UIView {
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        
        return tableView
    }()
    
    private var model: [RecipesListItemViewModel]?
        
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        tableView.dataSource = self
        tableView.delegate = self
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func bind(with model: [RecipesListItemViewModel]) {
        self.model = model
        tableView.reloadData()
    }
    
    private func setup() {
        setupTable()
    }
    
    private func setupTable() {
        addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension RecipesListView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = model else { return UITableViewCell() }
        let cell = RecipesListItemCell()
        cell.bind(with: model[indexPath.row])
        
        return cell        
    }
}

extension RecipesListView: UITableViewDelegate {
}

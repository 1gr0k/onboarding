//
//  RecipesListItemViewModel.swift
//  Onboarding
//
//  Created by Андрей Калямин on 29.05.2022.
//

import Foundation

struct RecipesListItemViewModel: Equatable {
    let id: String?
    let title: String
}

extension RecipesListItemViewModel {
    
    init(recept: Recipe) {
        self.title = recept.title ?? ""
        self.id = recept.id
    }
}

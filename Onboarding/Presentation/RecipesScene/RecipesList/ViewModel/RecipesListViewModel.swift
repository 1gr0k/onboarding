//
//  RecipesListViewModel.swift
//  Onboarding
//
//  Created by Андрей Калямин on 29.05.2022.
//

import Foundation

enum RecipesListViewModelLoading {
    case fullScreen
    case nextPage
}

protocol RecipesListViewModelDelegate: AnyObject {
    func fetchRecipes(didFetchRecipes items: [RecipesListItemViewModel])
}

protocol RecipesListViewModelInput {
    func viewDidLoad()
    func didLoadNextPage()
    func refresh()
    func setDelegate(with delegate: RecipesListViewModelDelegate)
}

protocol RecipesListViewModelOutput {
    var items: [RecipesListItemViewModel] { get }
    var query: String { get }
    var isEmpty: Bool { get }
    var screenTitle: String { get }
}

protocol RecipesListViewModel: RecipesListViewModelInput, RecipesListViewModelOutput {
    
}

final class DefaultRecipesListViewModel: RecipesListViewModel {
    
    static let itemsPerPage = 10
    
    private var recipesUseCase: RecipesListUseCase
    
    weak var delegate: RecipesListViewModelDelegate?
    
    var currentPage: Int = 0
    var totalPageCount: Int = 1
    
    var hasMorePages: Bool {
        return currentPage < totalPageCount
    }
    
    var nextPage: Int {
        return hasMorePages ? currentPage + 1 : currentPage }
    
    private var pages: [RecipesPage] = []
    private var recipesLoadTask: Cancellable? { willSet {recipesLoadTask?.cancel() } }
    
    var items: [RecipesListItemViewModel] = []
    
    var query: String = ""
    
    var isEmpty: Bool { return items.isEmpty }
    
    var screenTitle: String = "Рецепты"
    
    init(recipesUseCase: RecipesListUseCase) {
        self.recipesUseCase = recipesUseCase
    }
    
    func viewDidLoad() {
        query = "Pizza"
        update(receptQuery: RecipeQuery(query: query))
    }
    
    func didLoadNextPage() {
        guard hasMorePages else { return }
        load(receptQuery: .init(query: query), loading: .nextPage)
    }
    
    func refresh() {
        viewDidLoad()
    }
    
    func setDelegate(with delegate: RecipesListViewModelDelegate) {
        self.delegate = delegate
    }
    
    private func appendPage(_ recipesPage: RecipesPage) {
        currentPage = recipesPage.page
        totalPageCount = recipesPage.totalPages
        
        pages = pages
            .filter { $0.page != recipesPage.page } + [recipesPage]
        items = pages.recipes.map(RecipesListItemViewModel.init)
        delegate?.fetchRecipes(didFetchRecipes: items)
    }
    
    private func resetPages() {
        currentPage = 0
        totalPageCount = 1
        pages.removeAll()
        items.removeAll()
    }
    
    private func load(receptQuery: RecipeQuery, loading: RecipesListViewModelLoading) {
        query = receptQuery.query
        
        recipesLoadTask = recipesUseCase.execute(
            requestValue: .init(query: receptQuery, page: nextPage),
            completion: { result in
                switch result {
                case .success(let page):
                    self.appendPage(page)
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    private func update(receptQuery: RecipeQuery) {
        resetPages()
        load(receptQuery: receptQuery, loading: .fullScreen)
    }
    
    func update() {
        update(receptQuery: RecipeQuery(query: query))
    }
}

private extension Array where Element == RecipesPage {
    var recipes: [Recipe] { flatMap { $0.recipes } }
}
